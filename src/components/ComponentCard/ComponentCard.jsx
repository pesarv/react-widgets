import React, { useState, useEffect, useRef } from 'react'
import { NavLink } from 'react-router-dom'
import './component-card.scss'
import classNames from "classnames";

export default function ComponentCard({link = '/', title, component, children}) {

  const [scale, setScale] = useState(1);
  const [preload, setPreload] = useState(true);
  const previewContainer = useRef(0)

  useEffect(() => {
    setSize();
    window.addEventListener('resize', setSize);
    setTimeout(() => {
      setPreload(false)
    })
    return () => {
      window.removeEventListener('resize', setSize);
    }
  }, [])

  const setSize = (e, coefficient = 0.5) => {
    const preview = previewContainer.current;
    const previewWidth =  preview.clientWidth;
    const parentWidth = preview.parentElement.clientWidth;
    setScale(parentWidth / previewWidth * coefficient);
  }

  return (
    <NavLink to={link} className="component-card" onMouseEnter={() => { setSize(null, 0.6) }} onMouseLeave={() => { setSize() }}>
      <div className="component-card__container">
        <div className={classNames("component-card__container-preview", {"preload": preload})} style={{ transform: `translate(-50%, -50%) scale(${scale})` }} ref={previewContainer}>
          {component}
        </div>
      </div>
      <h2 className="component-card__title">{title}</h2>
      {/* <p className="component-card__info"> 
        {children}
      </p> */}
    </NavLink>
  )
}
