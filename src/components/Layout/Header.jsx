import React from 'react'
import Logo from './../../assets/logo.svg'
import { Link } from "react-router-dom";

export default function Header({children}) {
  return (
    <div className="header">
      <Link to="/">
        <img className="header__logo" alt="Logo "src={Logo}></img>
      </Link>

      <div>
        {children}
      </div>
    </div>
  )
}
