import React, {useState} from 'react';

import Slider from './../components/Slider/Slider';

export default {
  title: 'Slider',
  component: Slider,
  argTypes: {
    c1: { control: 'color' },
    c2: { control: 'color' },
  },
};

const Template = (args) => {
  const [value, setvalue] = useState(0)
  return <Slider {...args} onChange={(val) => {setvalue(val)}} value={value}/>;
}


export const Default = Template.bind({});
Default.args = {
  c1: '#ffffff',
  c2: '#ff0066'
};