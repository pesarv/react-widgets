import React from 'react'

export default function NotFound({el}) {
  return (
    <div className="not-found">
      <h1>404</h1>
    <p>{el && `${el} `}not found</p>
    </div>
  )
}
