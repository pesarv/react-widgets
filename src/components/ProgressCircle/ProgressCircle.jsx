import React from 'react'
import Defs from "./../../defs";
import './progress-circle.scss'

export default function ProgressCircle({radius = 100, strokeWidth = 20, c1 = "white", c2 = "white", value = 0}) {

  const c = (radius - strokeWidth / 2) * 2 * Math.PI;

  return (
    <div className="loader loader__container">
          <svg height={radius * 2} width={radius * 2}>
            <Defs c1={c1} c2={c2} />
            <circle
              cx={radius}
              cy={radius}
              r={radius - strokeWidth / 2}
              strokeWidth={strokeWidth - 2}
              stroke="black"
              fill="transparent"
            />
            <circle
              cx={radius}
              cy={radius}
              r={radius - strokeWidth / 2}
              strokeWidth={strokeWidth}
              stroke="url(#linear)"
              fill="transparent"
              strokeDasharray={`${c},${c}`}
              strokeDashoffset={c - (c / 100) * value}
              className="loader__circle"
              strokeLinecap="round"
            />
            <text
              x="50%"
              y="50%"
              dominantBaseline="middle"
              textAnchor="middle"
              fill="url(#linear-text)"
              className="loader__info"
            >
              {value}
            </text>
          </svg>
        </div>
  )
}
