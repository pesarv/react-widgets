import Home from "./Home";
import Credits from "./Credits";
import Components from "./Components";
import NotFound from "./NotFound";
import Carousel from "./Carousel";

export { Home, Credits, Components, NotFound, Carousel };