import React, { useState } from "react";
import { Switch, Route, useRouteMatch, withRouter } from "react-router-dom";
import {
  ColorPicker,
  ProgressCircle,
  Slider,
  Button,
  TreeView,
  TreeItem,
  ActionMenu,
  ActionMenuItem,
  Input,
} from "./../components";
import NotFound from "./NotFound";
import "./components.scss";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { componentRoutes } from "./../ComponentRoutes";

const initialState = {
  colorPickerValue: "#ff0066",
  progressCircleValue: 50,
  progressCircleStroke: 25,
  progressCircleRadius: 130,
  sliderValue: 0,
};

const Components = withRouter(({ location }) => {
  let { path } = useRouteMatch();

  const previousIndex = location.state?.previousPath
    ? componentRoutes.indexOf(location.state.previousPath)
    : 0;
  const currentIndex = componentRoutes.indexOf(location.pathname);
  const [state, setstate] = useState(initialState);

  const classes = `slide-${
    currentIndex < previousIndex ? "backward" : "forward"
  } slide`;

  return (
    <TransitionGroup
      className="component-animation"
      childFactory={(child) =>
        React.cloneElement(child, {
          classNames: classes,
        })
      }
    >
      <CSSTransition
        key={location.pathname}
        classNames={classes}
        timeout={1000}
      >
        <div className="component-animation__content">
          <Switch location={location}>
          <Route path={`${path}/actionmenu`}>
            <ActionMenu>
              <ActionMenuItem/>
            </ActionMenu>
            </Route>
            <Route path={`${path}/button`}>
              <div className="button-container">
                <Button>Button</Button>{" "}
                <Button theme="secondary">Button</Button>
              </div>
              <div className="button-container button-container--dark">
                <Button light>Button</Button>{" "}
                <Button light theme="secondary">
                  Button
                </Button>
              </div>
            </Route>
            <Route path={`${path}/colorpicker`}>
              <div className="color-example">
                <span style={{ background: state.colorPickerValue }}></span>
              </div>
              <ColorPicker
                value={state.colorPickerValue}
                onChange={(value) => {
                  setstate({ ...state, colorPickerValue: value });
                }}
                label="Color"
              ></ColorPicker>
            </Route>
            <Route path={`${path}/input`}>
              <Input></Input>
            </Route>
            <Route path={`${path}/progresscircle`}>
              <div className="progress-circle-container">
                <ProgressCircle
                  value={state.progressCircleValue}
                  c1="#ff0066"
                  c2="#ff0066"
                  strokeWidth={state.progressCircleStroke}
                  radius={state.progressCircleRadius}
                ></ProgressCircle>
              </div>
              <Slider
                width="300px"
                c1="black"
                c2="#ff0066"
                value={state.progressCircleValue}
                onChange={(value) =>
                  setstate({ ...state, progressCircleValue: value })
                }
              ></Slider>
              <Slider
                width="300px"
                min={5}
                max={45}
                c1="black"
                c2="#ff0066"
                value={state.progressCircleStroke}
                onChange={(value) =>
                  setstate({ ...state, progressCircleStroke: value })
                }
              ></Slider>
              <Slider
                width="300px"
                min={60}
                max={200}
                c1="black"
                c2="#ff0066"
                value={state.progressCircleRadius}
                onChange={(value) =>
                  setstate({ ...state, progressCircleRadius: value })
                }
              ></Slider>
            </Route>
            <Route path={`${path}/slider`}>
              <h2>{state.sliderValue}</h2>
              <Slider
                width="300px"
                c1="black"
                c2="#ff0066"
                value={state.sliderValue}
                onChange={(value) => setstate({ ...state, sliderValue: value })}
              ></Slider>
            </Route>
            <Route path={`${path}/treeview`}>
              <TreeView>
                <TreeItem label="Submenu">
                  <TreeItem label="Item"></TreeItem>
                  <TreeItem label="Item"></TreeItem>
                  <TreeItem label="Item"></TreeItem>
                </TreeItem>
                <TreeItem label="Item"></TreeItem>
                <TreeItem label="Item"></TreeItem>
                <TreeItem label="Item"></TreeItem>
              </TreeView>
            </Route>
            <Route path="*">
              <NotFound el="component" />
            </Route>
          </Switch>
        </div>
      </CSSTransition>
    </TransitionGroup>
  );
});

export default Components;
