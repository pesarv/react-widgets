import React from 'react'
import "./button.scss"
import classNames from 'classnames';

export default function Button({theme = 'primary', light, children}) {
  return (
    <button className={classNames(`button button--${theme}`, {"button--light": light})}>
      {children}
    </button>
  )
}
