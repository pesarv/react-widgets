import React from "react";
import {
  Slider,
  ColorPicker,
  ProgressCircle,
  ComponentCard,
  Button,
  TreeView,
  TreeItem,
} from "./../components";
import Logo from "./../logo";

export default function Home() {
  return (
    <div className="home">
      <div className="home__logo">
        <div className="home-logo">
          <Logo color="#ff0066" width="100%"></Logo>
          <h2>Spinner components</h2>
        </div>
      </div>

      <div className="home__components">
        <div className="components-grid">
          <ComponentCard
            link="/components/progresscircle"
            title="Progress circle"
            component={
              <ProgressCircle
                value={50}
                c1="#ff0066"
                c2="#ff0066"
                strokeWidth={15}
                radius={100}
              ></ProgressCircle>
            }
          >
            Indicates the progress
          </ComponentCard>
          <ComponentCard
            link="/components/slider"
            title="Slider"
            component={<Slider value={100} c1="#ff0066" c2="#ff0066"></Slider>}
          ></ComponentCard>
          <ComponentCard
            link="/components/colorpicker"
            title="Colorpicker"
            component={
              <ColorPicker value="#ff0066" label="Color"></ColorPicker>
            }
          >
            Pick color
          </ComponentCard>
          <ComponentCard
            link="/components/button"
            title="Button"
            component={<Button>Button</Button>}
          ></ComponentCard>
          <ComponentCard
            link="/components/treeview"
            title="Treeview"
            component={
              <TreeView c2="#ff0066">
                <TreeItem open>
                  <TreeItem></TreeItem>
                  <TreeItem></TreeItem>
                </TreeItem>
                <TreeItem></TreeItem>
                <TreeItem></TreeItem>
              </TreeView>
            }
          ></ComponentCard>
        </div>
      </div>
    </div>
  );
}
