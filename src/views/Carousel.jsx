import React, { useEffect, useState } from "react";
import "./carousel.scss";
import classNames from 'classnames';

const items = [{}, {}, {}, {}, {}, {}];
const itemGap = 360 / items.length;

export default function Carousel() {

  const [rotation, setRotation] = useState(0);
  const [activeItem, setActiveItem] = useState(0);
  const [animate, setAnimate] = useState(false);

  const changeItem = (index, itemRotation) => {
    if (activeItem !== index) {
      setActiveItem(index);
      setRotation(itemRotation);
      setAnimate(true);
      setTimeout(() => {
        setAnimate(false)
      }, 1500)
    }
  }

  const carouselC = 290 * 2 * Math.PI;

  return (
    <div className={classNames("carousel")}>
      <svg height="600" width="600" viewBox="0 0 600 600">
        <circle cx="50%" cy="50%" className={classNames("carousel__circle")} style={{ strokeDasharray: carouselC / 1000  }}></circle>
        <circle cx="50%" cy="50%" className={classNames("carousel__circle", { "carousel__circle--hidden": animate })}></circle>
      </svg>
      {items.map((item, index) => {
        const itemRotation = itemGap * index - rotation
        return (
          <span
            className={classNames("carousel-item", {"carousel-item--active": index === activeItem})}
            style={{
              transform: `rotate(${itemRotation}deg) translateY(-290px) rotate(${itemRotation * -1}deg)`,
            }}
            onClick={() => {
              changeItem(index, itemGap * index);
            }}
          >
            {index}
          </span>
        );
      })}
    </div>
  );
}
