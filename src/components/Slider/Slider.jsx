import React, { useState, useRef }  from "react";
import "./slider.scss";

export default function Slider({onChange, min = 0, max = 100,  c1 = "black", c2 = "black", width = "100%", value}) {
  const [isMoving, setisMoving] = useState(false);
  const [percentage, setPercentage] = useState((value - min) / (max - min) * 100);

  const sliderRef = useRef();
  const thumbRef = useRef();

  const md = (e) => {
    e.preventDefault();
    if (e.button !== 0) {
      return;
    }
    thumbRef.current.focus();
    setisMoving(true);
    document.addEventListener("mouseup", mu);
  };

  const mu = () => {
    setisMoving(false);
    document.removeEventListener("mouseup", mu);
  }

  const mm = (e) => {
    if(!isMoving) {
      return
    }
    const el = sliderRef.current;
    const sliderWidth = el.getBoundingClientRect().width;
    const mouseLeft = e.pageX - (el.getBoundingClientRect().x);
    const p = Math.round(mouseLeft / sliderWidth * 100) > 100 ? 100 : Math.round(mouseLeft / sliderWidth * 100) < 0 ? 0 : Math.round(mouseLeft / sliderWidth * 100);
    setPercentage(p);
    const value = Math.round(min + (max - min) / 100 * p);
    onChange(value);
  };

  return (
    <div className="slider__root" style={{ width: width }} ref={sliderRef} onMouseDown={(e) => md(e)} onMouseMove={(e) => mm(e)}>
      <div className="slider__container">
        <div className="slider__rail" style={{right: `${100 - percentage}%`, backgroundImage: `linear-gradient(90deg,${c1},${c2})`}}></div>
        <div className="slider__thumb" ref={thumbRef} style={{left: `${percentage}%`, background: c2}} tabIndex="0"></div>
        <input type="hidden" value={value}></input>
      </div>
    </div>
  );
}
