import React from 'react';

import ProgressCircle from './../components/ProgressCircle/ProgressCircle';

export default {
  title: 'Progress circle',
  component: ProgressCircle,
  argTypes: {
    c1: { control: 'color' },
    c2: { control: 'color' },
    strokeWidth: { control: 'number' },
    radius: { control: 'number' },
    value: { 
      control: {
        type: 'range',
        min: 0,
        max: 100
      }
    }
  },
};

const Template = (args) => <ProgressCircle {...args} />;


export const Default = Template.bind({});
Default.args = {
  value: 75,
  c1: '#ffffff',
  c2: '#ff0066'
};