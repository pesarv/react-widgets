import React from "react";
import {
  Switch,
  Route,
  useRouteMatch,
  useLocation
} from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { Home, Credits, Components, NotFound, Carousel } from "./views";

export default function AppRoutes() {
  const match = useRouteMatch({
    path: "/:key",
  });
  const location = useLocation()

  return (
    <TransitionGroup className="app-routes-animation">
      <CSSTransition key={match?.params.key} classNames="fade" timeout={1000}>
        <div className="app-routes-animation__content">
          <Switch location={location}>
            <Route path="/components">
              <Components />
            </Route>
            <Route path="/carousel">
              <Carousel />
            </Route>
            <Route path="/credits">
              <Credits />
            </Route>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </div>
      </CSSTransition>
    </TransitionGroup>
  );
}
