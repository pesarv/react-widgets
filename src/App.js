import React from "react";
import "./App.scss";
import {
  Layout,
  Header,
  Sidebar,
  TreeView,
  TreeItem,
  Main,
} from "./components";
import AppRoutes from "./AppRoutes";
import { withRouter } from "react-router-dom";
import { componentRoutes, componentLabels } from './ComponentRoutes'; 

const App = withRouter(({location}) => {
  return (
      <Layout>
        <Header></Header>
        <Sidebar>
          <TreeView c1="white" c2="#ff0066">
            <TreeItem label="Components">
              {componentRoutes.map((route) => {
                return <TreeItem label={componentLabels[route]} link={{pathname: route, state: { previousPath: location.pathname }}} key={componentRoutes.indexOf(route)}></TreeItem>
              })}
            </TreeItem>
            <TreeItem label="Carousel" link="/carousel"></TreeItem>
            <TreeItem label="Storybook"></TreeItem>
            <TreeItem label="Credits" link="/credits"></TreeItem>
          </TreeView>
        </Sidebar>
        <Main>
          <AppRoutes></AppRoutes>
        </Main>
      </Layout>
  );
});

export default App;
