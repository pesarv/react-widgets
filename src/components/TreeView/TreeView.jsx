import React, { useState, useEffect } from "react";
import './tree-view.scss';
import classNames from "classnames";

export default function TreeView({c1 = "black", c2 = "white", cActive = "white", closed, ...props}) {


  const children = props.children
  ?.filter((c) => c.type.name === "TreeItem")
  .map((c, index) => {
    const child = React.cloneElement(c, {
      first: index === 0,
      last: index === props.children.length - 1,
      c1,
      openProp: c.openProp,
      c2,
      key: index,
    });
    return child;
  })

  const [rendered, setRendered] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setRendered(true)
    },1000);
  }, [])

  return (
    <div className={classNames("tree-view", {"tree-view--closed": closed, "tree-view--preload": !rendered})}>
      {children}
    </div>
  );
}
