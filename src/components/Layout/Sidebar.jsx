import React from 'react'
import { useState } from 'react';
import ChevronRight from '@icons/material/ChevronRightIcon';
import ChevronLeft from '@icons/material/ChevronLeftIcon';
import { cloneElement } from 'react';
import classNames from "classnames";

export default function Sidebar({children}) {

  const [open, setopen] = useState(false)

  return (
    <div className={classNames("sidebar", {'sidebar--open': open})}>
      <div className="sidebar__content">
        { cloneElement(children, {closed: !open}) }
      </div>
      <div className="sidebar__icon" onClick={() => setopen(!open)}>
        {open ? <ChevronLeft></ChevronLeft> : <ChevronRight></ChevronRight>}
      </div>
    </div>
  )
}
