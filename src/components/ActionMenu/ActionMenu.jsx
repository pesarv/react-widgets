import React, { useState } from "react";
import "./action-menu.scss";
import classNames from "classnames";

export default function ActionMenu({ children }) {
  const [isOpen, setIsOpen] = useState(false);

  const items = [
    {
      label: 'Label'
    },
    {
      label: 'Label'
    },
    {
      label: 'Label'
    },
    {
      label: 'Label'
    },    
  ];
  const childrenCount = items.length;
  const transitionInterval = childrenCount > 4 ? 0.10 : 0.25; 
  const itemRotations = [...Array(childrenCount).keys()].map(index => 45 * Math.ceil(index / 2) * (index % 2 === 0 ? 1 : -1)).sort((a, b) => a - b).map(val => val + (childrenCount % 2 === 0 ? 292.5 :  270));
  console.log(itemRotations)

  return (
    <div className="action-menu">
      {items.map((item, index) => {
        const rotation = itemRotations[index]
        
        return (
          <div className={classNames("am-child", {"visible": isOpen})} style={{transform: isOpen ? `rotate(${rotation}deg) translateX(100px) rotate(-${rotation}deg)` : 'translateX(0px)', transitionDelay: `${transitionInterval * (isOpen ? index : childrenCount - 1 - index)}s`}}>
            {index}
          </div>
        )
      })}
      <button
        className="action-menu__button"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <div
          className={classNames("am-btn-icon", {
            "am-btn-icon--close": isOpen,
          })}
        >
          <span className="am-btn-icon-line am-btn-icon-line--top"></span>
          <span className="am-btn-icon-line am-btn-icon-line--center"></span>
          <span className="am-btn-icon-line am-btn-icon-line--bottom"></span>
        </div>
      </button>
      <svg viewBox="0 0 150 150" className={classNames("am-btn-ripple", {active: isOpen})} height="150" width="150">
        {items.map((item, index) => {
          return <circle cx="75" cy="75" style={{animationDelay: `${transitionInterval * index}s`}} key={index}></circle>
        })}
      </svg>
    </div>
  );
}
