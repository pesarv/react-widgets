import React, { useState } from "react";
import "./tree-item.scss";
import classNames from "classnames";
import { NavLink } from "react-router-dom";
import { CSSTransition } from 'react-transition-group';

export default function TreeItem({c1, c2, cActive, link, ...props}) {

  const children = props.children
  ?.filter((c) => c.type.name === "TreeItem")
  .map((c, index) => {
    const child = React.cloneElement(c, {
      first: index === 0,
      last: index === props.children.length - 1,
      c1,
      c2,
      key: index
    });
    return child;
  })
  const [open, setOpen] = useState(false);
  const lineLength = Math.sqrt(48*48 + 48*48);
  
 

  return (
    <div
      className={classNames("tree-item", {
        "tree-item--first": props.first,
        "tree-item--last": props.last,
      })}
    >
      <div className={classNames("tree-item__label", {"tree-item__label-link": children})} onClick={children ? () => {setOpen(!open)} : undefined}>
        <div className="tree-item__label-icon">
          <span className="icon" style={{backgroundColor: !children ? c2 : open ? c2 : c1, border: `2px solid ${c2}` }}></span>
          <span
            className={classNames("graph", {
              "graph--bottom": props.first,
              "graph--top": props.last,
            })}
            style={{background: c1}}
          ></span>
          {children && (
            <svg className={classNames("diagonal-graph")}>
              <line
                x1="0"
                y1="0"
                x2="100%"
                y2="100%"
                style={{ stroke: c1, strokeWidth: 2, strokeDasharray: `${lineLength}, ${lineLength}`, strokeDashoffset: open ? 0 : lineLength }}
              ></line>
            </svg>
          )}
        </div>
        <LabelWrapper link={link}>
          <span className={classNames("tree-item__label-text", {open: open})} style={{color: c1}}>{props.label}</span>
        </LabelWrapper>
      </div>
      {children &&
          <div className={classNames("tree-item__children")}>
            <span className="tree-item__children-tree">
              {!props.last && <span className="graph" style={{background: c1}}></span>}
            </span>
            <CSSTransition in={open} timeout={2000} classNames="expand">
              <div className="children-container">{children}</div>
            </CSSTransition>
          </div>}
    </div>
  );
}

const LabelWrapper = ({link, children}) => {
  return link ? <NavLink to={link} className="tree-item__label-link">{children}</NavLink> : children
}
