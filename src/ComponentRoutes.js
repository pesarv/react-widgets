export const componentRoutes = [
  '/components/actionmenu',
  '/components/button',
  '/components/colorpicker',
  '/components/input',
  '/components/progresscircle',
  '/components/slider',
  '/components/treeview',
]

export const componentLabels = {
  '/components/actionmenu': 'Action menu',
  '/components/button': 'Button',
  '/components/colorpicker': 'Colorpicker',
  '/components/input': 'Input',
  '/components/progresscircle': 'Progress circle',
  '/components/slider': 'Slider',
  '/components/treeview': 'Tree view',
}