import React, { useRef, useState, useEffect } from "react";
import "./colorPicker.scss";
import classNames from "classnames";

export default function ColorPicker({ label, onChange, value }) {
  const inputRef = useRef();
  const [preload, setPreload] = useState(true);
  const [c, setc] = useState(0);

  useEffect(() => {
   checkSize();
   setTimeout(() => {
    setPreload(false)
  })
  }, []);

  const openInput = () => {
    const input = inputRef.current;
    checkSize();
    input.click();
  };

  const checkSize = () => {
    const input = inputRef.current;
    setc(
      2 * input.clientHeight +
        2 * input.clientWidth
    );
  }

  const colorChange = (newColor) => {
    onChange(newColor);
  };

  return (
    <div className={classNames("colorpicker", {"preload": preload})}>
      <button
        className="colorpicker__container"
        // style={{boxShadow: `0 0 2px 4px inset ${color}`}}
        onClick={() => {
          openInput();
        }}
      >
        <svg>
          <rect
            className="colorpicker__border"
            rx="5"
            stroke={value}
            x="1"
            y="1"
            strokeDasharray={c}
            strokeDashoffset={c}
          ></rect>
        </svg>
        <span
          className="colorpicker__color"
          style={{ background: value }}
        ></span>
        {label && (
          <span className="colorpicker__label" style={{ color: value }}>
            {label}
          </span>
        )}
      </button>
      <input
        type="color"
        value={value}
        className="colorpicker__input"
        ref={inputRef}
        onChange={(e) => {
          colorChange(e.target.value);
        }}
      ></input>
    </div>
  );
}
